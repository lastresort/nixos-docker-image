{ pkgs, ... }:
{
  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    virtualHosts."localhost".locations."/".root = "/var/www";
  };

  environment.systemPackages = with pkgs; [
    nginx
  ];
}
