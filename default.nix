let
  nixpkgs = import <nixpkgs> {};
  system = nixpkgs.nixos.system.buildSystem {
    system = "x86_64-linux";
    modules = [
      ./configuration.nix
      { config, lib, pkgs, ... }: {
        boot.isContainer = true;
        networking.hostName = "nginx";
      }
    ];
  };
  config = system.config;
  storePaths = nixpkgs.stdenv.mkDerivation {
    name = "store-paths";
    buildInputs = [ nixpkgs.closureInfo ];
    buildCommand = ''
      export referencesClosure=1
      exec ${nixpkgs.closureInfo}/bin/dump-path \
        $out ${config.system.build.toplevel}
    '';
  };
  image = nixpkgs.dockerTools.buildImage {
    name = "nixos-nginx";
    contents = [ storePaths ];
    config = {
      Cmd = [ "${config.system.build.toplevel}/sw/bin/nginx" ];
    };
  };
in
  image
